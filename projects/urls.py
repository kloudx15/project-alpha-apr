from django.urls import path
from projects.views import (
    ProjectDetailView,
    ProjectListView,
    ProjectCreateView,
    ProjectUpdateView,
    ProjectDeleteView,
)

urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
    path("<int:pk>/", ProjectDetailView.as_view(), name="show_project"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
    path(
        "<int:pk>/update/", ProjectUpdateView.as_view(), name="project_update"
    ),
    path(
        "<int:pk>/delete/", ProjectDeleteView.as_view(), name="project_delete"
    ),
]
